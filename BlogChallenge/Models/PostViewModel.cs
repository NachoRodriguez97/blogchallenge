﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BlogChallenge.Models
{
    public class PostViewModel
    {
        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        [Display(Name = "Identificador")]
        public string Id { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        public string Titulo { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        public string Contenido { get; set; }

        public string Imagen { get; set; }

        public string Categoria { get; set; }

        public DateTime FechaDeCreacion { get; set; }

        public bool Activo { get; set; }
    }
}
