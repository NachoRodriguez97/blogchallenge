﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using BlogChallenge.Models;
using Challenge.Database;

namespace BlogChallenge.Controllers
{
    public class PostsController : Controller
    {
        private readonly BlogDbContext _context;

        public PostsController(BlogDbContext context)
        {
            _context = context;
        }

        public async Task<IActionResult> GetAll()
        {
            return View(await _context.Posts.Where(p => p.Activo == true).OrderByDescending(p => p.FechaDeCreacion).ToListAsync());
        }

        public async Task<IActionResult> Home()
        {
            return await GetAll();
        }

        public async Task<IActionResult> Index()
        {
            return View(await _context.Posts.ToListAsync());
        }

        public async Task<IActionResult> GetById(string id)
        {
            if (id == null)
            {
                return RedirectToAction(nameof(Home));
            }

            var post = await _context.Posts
                .FirstOrDefaultAsync(m => m.Id.Equals(id));
            if (post == null)
            {
                return RedirectToAction(nameof(Home));
            }

            return View(post);
        }

        public async Task<IActionResult> Details(string id)
        {
            return await GetById(id);
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(PostViewModel post)
        {
            var p = _context.Posts.FirstOrDefault(x => x.Id.Equals(post.Id));

            if (p != null)
            {
                ModelState.AddModelError(string.Empty, "El id esta en uso. Escriba otro");
                return View("Create", post);
            }
            if (ModelState.IsValid && p == null)
            {
                post.FechaDeCreacion = DateTime.Now;
                post.Activo = true;
                _context.Add(post);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Home));
            }
            return View(post);
        }

        public IActionResult FormularioDeEdicion()
        {
            return View();
        }

        public async Task<IActionResult> Update(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var postViewModel = await _context.Posts.FindAsync(id);
            if (postViewModel == null)
            {
                return NotFound();
            }
            return View(postViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Update(string id, PostViewModel postViewModel)
        {
            if (id != postViewModel.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    var post = _context.Posts.FirstOrDefault(p => p.Id == id);
                    post.Titulo = postViewModel.Titulo;
                    post.Contenido = postViewModel.Contenido;
                    post.FechaDeCreacion = DateTime.Now;

                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PostViewModelExists(postViewModel.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Home));
            }
            return View(postViewModel);
        }

        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var postViewModel = await _context.Posts
                .FirstOrDefaultAsync(m => m.Id == id);
            if (postViewModel == null)
            {
                return NotFound();
            }

            return View(postViewModel);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var postViewModel = await _context.Posts.FindAsync(id);
            postViewModel.Activo = false;
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Home));
        }

        private bool PostViewModelExists(string id)
        {
            return _context.Posts.Any(e => e.Id == id);
        }
    }
}
